import 'package:bedatoko/constant/theme/ThemeColor.dart';
import 'package:bedatoko/constant/theme/icon.dart';
import 'package:bedatoko/constant/theme/textStyle.dart';
import 'package:bedatoko/provider/google_sign_in.dart';
import 'package:bedatoko/widgets/button/textButton.dart';
import 'package:bedatoko/widgets/textField/RoundedTextField.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginView extends StatefulWidget {
  const LoginView({
    Key key,
  }) : super(key: key);
  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  TextEditingController username;
  TextEditingController password;
  FocusNode usernameNode;
  FocusNode passwordNode;

  @override
  void initState() {
    username = TextEditingController();
    password = TextEditingController();
    usernameNode = FocusNode();
    passwordNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    username.dispose();
    usernameNode.dispose();
    password.dispose();
    passwordNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final node = FocusScope.of(context);
    return ListTile(
      contentPadding:
          EdgeInsets.only(top: 8.0, left: 0, right: 0, bottom: 16.0),
      title: Row(
        children: [
          Expanded(
            flex: 3,
            child: Container(
              padding: EdgeInsets.only(left: 16, bottom: 8.0),
              decoration: BoxDecoration(
                  border: Border(
                      bottom:
                          BorderSide(color: themeColor.secondary, width: 1.0))),
              child: Text(
                'Sign in',
                style: ThemeText.theme.headline6.copyWith(
                  color: themeColor.text,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: SizedBox(),
          )
        ],
      ),
      subtitle: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 32),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 35,
              ),
              RoundedTextField(
                  controller: username,
                  placeholder: 'username',
                  textInputAction: TextInputAction.next,
                  icon: ImageIcons.user,
                  focus: usernameNode,
                  onEditingComplete: () {
                    node.nextFocus();
                  }),
              SizedBox(
                height: 15,
              ),
              RoundedTextField(
                  controller: password,
                  placeholder: 'password',
                  password: true,
                  focus: passwordNode,
                  icon: ImageIcons.lock,
                  textInputAction: TextInputAction.done,
                  onEditingComplete: () => passwordNode.unfocus()),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                    onTap: () {},
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'Forgot Password ?',
                        style: ThemeText.theme.caption
                            .copyWith(color: themeColor.text.withOpacity(0.6)),
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 14,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  KTextButton(
                    text: 'Login',
                    onPressed: () {},
                  ),
                ],
              ),
              SizedBox(
                height: 18,
              ),
              KButton(
                icon: ImageIcons.google,
                borderWidth: 1.0,
                borderColor: themeColor.primary,
                color: themeColor.backgroundAccent,
                textColor: themeColor.text,
                text: 'Sign in with Google',
                onPressed: () {
                  final provider =
                      Provider.of<GoogleSignInProvider>(context, listen: false);
                  provider.login();
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
