import 'dart:async';

import 'package:bedatoko/constant/theme/radius.dart';
import 'package:bedatoko/constant/theme/theme.dart';
import 'package:bedatoko/module/auth/login/login.dart';
import 'package:bedatoko/module/auth/register/register.dart';
import 'package:bedatoko/provider/google_sign_in.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

import 'widgets/get_started_btn.dart';

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage>
    with TickerProviderStateMixin {
  AnimationController _getStartedAnimController;
  PageController _pageController;
  StreamController _fadeStreamController = StreamController<double>();
  bool registerMode = false;
  Stream _fadeStream;
  double _fadeTrigger = 200;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(
      initialPage: 0,
      keepPage: true,
    );

    _fadeStream = _fadeStreamController.stream.asBroadcastStream();

    _pageController.addListener(() {
      _fadeStreamController
          .add(fadeTrigger(_pageController.offset, _fadeTrigger));
    });
    _getStartedAnimController = AnimationController(
      duration: Duration(milliseconds: 1000),
      vsync: this,
    )..repeat(reverse: true);
  }

  double fadeTrigger(double pageOffset, double trigger) {
    if (pageOffset < trigger) {
      return pageOffset / trigger;
    } else {
      return 1.0;
    }
  }

  @override
  void dispose() {
    _fadeStreamController.close();
    _getStartedAnimController.dispose();
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () {
        // print(_pageController.page);
        if (_pageController.page == 1.0) {
          _pageController.animateToPage(0,
              duration: Duration(milliseconds: 750), curve: Curves.easeInOut);
        }
        return null;
      },
      child: Scaffold(
        backgroundColor: themeColor.primary,
        body: Container(
          color: themeColor.primary,
          // decoration: BoxDecoration(gradient: ThemeGradient.background),
          child: Stack(
            children: [
              StreamBuilder<double>(
                  stream: _fadeStream,
                  builder: (context, snapshot) {
                    double value = 0;
                    if (snapshot.hasData) {
                      value = snapshot.data;
                    }
                    return Positioned(
                      top: 24,
                      left: 0,
                      right: 0,
                      child: Opacity(
                          opacity: 1.0 - value,
                          child: Image.asset('assets/image/welcome_01.png')),
                    );
                  }),
              StreamBuilder<double>(
                  stream: _fadeStream,
                  builder: (context, snapshot) {
                    double value = 0;
                    if (snapshot.hasData) {
                      value = snapshot.data;
                    }
                    return Positioned(
                      bottom: size.height / 4,
                      child: Opacity(
                        opacity: 1.0 - value,
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 64),
                          width: size.width,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                'Welcome to XXXX',
                                style: ThemeText.titleL,
                              ),
                              SizedBox(
                                height: 16,
                              ),
                              Text(
                                'Choosing the cheapest products from the most near distance and help to effect your shopping budget',
                                style: ThemeText.text.copyWith(
                                    fontWeight: FontWeight.w500,
                                    color: themeColor.backgroundAccent),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  }),
              PageView(
                controller: _pageController,
                physics: NeverScrollableScrollPhysics(),
                scrollDirection: Axis.vertical,
                children: [
                  SafeArea(
                    child: Stack(
                      children: [
                        Positioned(
                          left: 0,
                          right: 0,
                          bottom: 0,
                          child: GetStartedButton(
                              onPress: () {
                                _pageController.animateToPage(1,
                                    duration: Duration(milliseconds: 750),
                                    curve: Curves.easeInOut);
                              },
                              getStartedAnimController:
                                  _getStartedAnimController),
                        ),
                      ],
                    ),
                  ),
                  SingleChildScrollView(
                    child: Container(
                      height: size.height,
                      decoration:
                          BoxDecoration(gradient: ThemeGradient.background),
                      child: Stack(
                        children: [
                          Positioned(
                            top: 16,
                            left: 16,
                            child: FlatButton(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Icon(
                                    FontAwesomeIcons.arrowLeft,
                                    size: 16,
                                    color: themeColor.backgroundAccent,
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Text(
                                    'Back',
                                    style: ThemeText.titleS,
                                  )
                                ],
                              ),
                              onPressed: () async {
                                FocusManager.instance.primaryFocus.unfocus();
                                await Future.delayed(
                                    Duration(milliseconds: 200), () {
                                  _pageController.animateToPage(0,
                                      duration: Duration(milliseconds: 750),
                                      curve: Curves.easeInOut);
                                });
                                // if (_pageController.page == 1.0) {

                                // }
                              },
                            ),
                          ),
                          _CardStack(
                            offset: Offset(-4.0, 18.0),
                            child: Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                  margin: EdgeInsets.only(bottom: 4),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        registerMode
                                            ? "Have an account ?"
                                            : "Don't have an account?",
                                        style: ThemeText.theme.bodyText1,
                                      ),
                                      SizedBox(
                                        width: 8,
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            registerMode = !registerMode;
                                          });
                                        },
                                        child: Container(
                                          height: 30,
                                          child: Center(
                                            child: Text(
                                              registerMode
                                                  ? "Sign in"
                                                  : "Sign up ",
                                              style: ThemeText.theme.bodyText1
                                                  .copyWith(
                                                      color:
                                                          themeColor.secondary),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  )),
                            ),
                          ),
                          _CardStack(
                            offset: Offset(4.0, -18.0),
                            child: registerMode ? RegisterView() : LoginView(),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _CardStack extends StatelessWidget {
  final Offset offset;
  final Widget child;

  const _CardStack({Key key, this.offset, this.child}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double width = size.width * 0.8;
    double height = size.height * 0.5;
    return Positioned(
      top: ((size.height - height) / 2) + offset.dy,
      left: ((size.width - width) / 2) + offset.dx,
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          color: themeColor.background,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.2),
              blurRadius: 4.0,
              spreadRadius: 4.0,
              offset: Offset(-2.0, 4.0),
            )
          ],
          borderRadius: ThemeRadius.borderTopRight20,
        ),
        child: child,
      ),
    );
  }
}
