import 'package:bedatoko/constant/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class GetStartedButton extends StatelessWidget {
  const GetStartedButton({
    Key key,
    @required AnimationController getStartedAnimController,
    this.onPress,
  })  : _getStartedAnimController = getStartedAnimController,
        super(key: key);

  final AnimationController _getStartedAnimController;
  final Function onPress;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      onPanUpdate: (details) {
        if (details.delta.dy < 0) {
          onPress();
        }
      },
      child: Container(
        height: 150,
        color: Colors.transparent,
        child: AnimatedBuilder(
          animation: _getStartedAnimController,
          builder: (context, child) {
            return Transform.translate(
              offset: Offset(0.0, 10 * _getStartedAnimController.value),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Get Started',
                    style: ThemeText.titleS,
                  ),
                  Icon(
                    FontAwesomeIcons.chevronDown,
                    color: themeColor.backgroundAccent,
                    size: 12,
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
