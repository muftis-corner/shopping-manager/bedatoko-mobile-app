import 'package:bedatoko/constant/theme/icon.dart';
import 'package:bedatoko/constant/theme/theme.dart';
import 'package:bedatoko/widgets/button/textButton.dart';
import 'package:bedatoko/widgets/textField/RoundedTextField.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RegisterView extends StatefulWidget {
  final bool registerMode;
  final Function signUp;

  const RegisterView({Key key, this.registerMode, this.signUp})
      : super(key: key);
  @override
  _RegisterViewState createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _name = TextEditingController();
  TextEditingController _username = TextEditingController();
  TextEditingController _email = TextEditingController();
  TextEditingController _password = TextEditingController();
  TextEditingController _confirmPassword = TextEditingController();

  bool _success;
  String _userEmail;

  @override
  void dispose() {
    // name.dispose();
    // username.dispose();
    // email.dispose();
    // password.dispose();
    // confirmPassword.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    FocusScopeNode node = FocusScope.of(context);
    return ListTile(
      contentPadding:
          EdgeInsets.only(top: 8.0, left: 0, right: 0, bottom: 16.0),
      title: Row(
        children: [
          Expanded(
            flex: 3,
            child: Container(
              padding: EdgeInsets.only(left: 16, bottom: 8.0),
              decoration: BoxDecoration(
                  border: Border(
                      bottom:
                          BorderSide(color: themeColor.secondary, width: 1.0))),
              child: Text(
                'Sign up',
                style: ThemeText.theme.headline6.copyWith(
                  color: themeColor.text,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: SizedBox(),
          )
        ],
      ),
      subtitle: Column(
        children: [
          Container(
            height: size.height * 0.5 - (size.height * 0.061),
            child: CupertinoScrollbar(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 32),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 8,
                        ),
                        RoundedTextField(
                          controller: _name,
                          placeholder: 'Name',
                          icon: ImageIcons.user,
                          textInputAction: TextInputAction.next,
                          onEditingComplete: node.nextFocus,
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        RoundedTextField(
                          controller: _username,
                          placeholder: 'Username',
                          icon: ImageIcons.user,
                          textInputAction: TextInputAction.next,
                          onEditingComplete: node.nextFocus,
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        RoundedTextField(
                          controller: _email,
                          placeholder: 'Email',
                          icon: ImageIcons.message,
                          textInputAction: TextInputAction.next,
                          onEditingComplete: node.nextFocus,
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        RoundedTextField(
                          controller: _password,
                          placeholder: 'Password',
                          password: true,
                          icon: ImageIcons.lock,
                          textInputAction: TextInputAction.next,
                          onEditingComplete: node.nextFocus,
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        RoundedTextField(
                          controller: _confirmPassword,
                          placeholder: 'Confirm Password',
                          password: true,
                          icon: ImageIcons.lock,
                          textInputAction: TextInputAction.done,
                          onEditingComplete: node.unfocus,
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            KTextButton(
                              text: 'Register',
                              onPressed: () {},
                            ),
                          ],
                        ),
                        // SizedBox(
                        //   height: 10,
                        // ),
                        KButton(
                          icon: ImageIcons.google,
                          borderWidth: 1.0,
                          borderColor: themeColor.primary,
                          color: themeColor.backgroundAccent,
                          textColor: themeColor.text,
                          text: 'Sign up with Google',
                          onPressed: () {},
                        ),
                        SizedBox(
                          height: 8,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
