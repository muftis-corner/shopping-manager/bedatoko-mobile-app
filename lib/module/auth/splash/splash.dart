import 'package:bedatoko/constant/routes.dart';
import 'package:bedatoko/constant/theme/radius.dart';
import 'package:bedatoko/constant/theme/theme.dart';
import 'package:bedatoko/data/session.dart';
import 'package:bedatoko/module/auth/welcome/welcome.dart';
import 'package:bedatoko/module/dashboard/dashboard.dart';
import 'package:bedatoko/provider/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // Future.delayed(
    //     Duration(seconds: 3),
    //     () => {
    //           Navigator.pushNamedAndRemoveUntil(
    //               context, Routes.welcome, ModalRoute.withName(Routes.welcome))
    //         });
    // super.initState();
  }

  void getdata(User user) async {
    try {
      Session.token = await user.getIdToken();
      print(Session.token);
    } catch (error) {
      print('something went wrong !');
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Scaffold(
      backgroundColor: themeColor.primary,
      body: ChangeNotifierProvider(
        create: (context) => GoogleSignInProvider(),
        child: StreamBuilder<User>(
          stream: FirebaseAuth.instance.authStateChanges(),
          builder: (context, snapshot) {
            final provider = Provider.of<GoogleSignInProvider>(context);
            if (provider.isSigningIn) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Center(
                      child: FlutterLogo(
                    size: 200,
                  )),
                  Center(child: Loading()),
                ],
              );
            } else if (snapshot.hasData) {
              getdata(snapshot.data);
              return Dashboard();
            } else {
              return WelcomePage();
            }
          },
        ),
      ),
    );
  }
}

class Loading extends StatefulWidget {
  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> with TickerProviderStateMixin {
  List<AnimationController> listController = [];
  List<Animation<double>> listAnim = [];
  List<CurvedAnimation> listCurve = [];

  List<List> itemColor =
      List.generate(13, (index) => [index, themeColor.secondary]);

  @override
  void initState() {
    listController = itemColor
        .map((e) => AnimationController(
            vsync: this, duration: Duration(milliseconds: 500)))
        .toList();
    listCurve = listController
        .map((e) => CurvedAnimation(
            parent: e,
            curve: Curves.easeInOutSine,
            reverseCurve: Curves.linearToEaseOut))
        .toList();
    listAnim = listCurve
        .map((e) => Tween<double>(begin: 4, end: 12).animate(e))
        .toList();
    listAnimate();
    super.initState();
  }

  void listAnimate() async {
    for (final ctrl in listController) {
      await Future.delayed(Duration(milliseconds: 100), () {
        print('${listController.indexOf(ctrl)}');
        ctrl.repeat(reverse: true);
      });
    }
  }

  @override
  void dispose() {
    listController.forEach((element) {
      element.dispose();
    });
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 10,
      width: 8.0 * listAnim.length,
      decoration: BoxDecoration(
        color: Colors.transparent,
        borderRadius: ThemeRadius.all12,
      ),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: itemColor
              .map((e) => LoadingDot(
                    size: 6,
                    animation: listAnim[itemColor.indexOf(e)],
                    color: e[1],
                  ))
              .toList()),
    );
  }
}

class LoadingDot extends AnimatedWidget {
  final double size;
  final Color color;
  LoadingDot(
      {this.size,
      @required Animation<double> animation,
      this.color = Colors.grey})
      : super(listenable: animation);

  @override
  Widget build(BuildContext context) {
    final Animation<double> animation = super.listenable as Animation<double>;

    return Container(
      alignment: Alignment.bottomCenter,
      height: size * 2,
      child: Transform.translate(
        offset: Offset(0, animation.value * 2),
        child: Container(
          height: size,
          width: size,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: this.color,
            // shape: BoxShape.circle,
            borderRadius: ThemeRadius.all12,
          ),
        ),
      ),
    );
  }
}
