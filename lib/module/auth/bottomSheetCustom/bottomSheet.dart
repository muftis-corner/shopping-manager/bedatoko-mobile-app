import 'package:bedatoko/constant/theme/radius.dart';
import 'package:bedatoko/constant/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geolocator/geolocator.dart';

class BottomSheetStack extends StatefulWidget {
  final double height;
  // final Stream<double> swipeUpdate;
  final Color backgroundColor;
  final Widget child;

  const BottomSheetStack(
      {Key key,
      // @required this.swipeUpdate,
      @required this.height,
      this.child,
      this.backgroundColor})
      : super(key: key);

  @override
  BottomSheetStackState createState() => BottomSheetStackState();
}

class BottomSheetStackState extends State<BottomSheetStack>
    with TickerProviderStateMixin {
  AnimationController _slidingAnimationCtrl;
  AnimationController _backBtnAnimCtrl;
  Animation<double> _backBtnAnim;
  Animation<double> _slidingAnim;

  Color _backgroundColor;

  double bottom;

  @override
  void initState() {
    super.initState();
    _backgroundColor = widget.backgroundColor ?? themeColor.background;
    _slidingAnimationCtrl =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _slidingAnim =
        Tween<double>(begin: widget.height, end: 0).animate(CurvedAnimation(
      parent: _slidingAnimationCtrl,
      curve: Curves.easeInSine,
      reverseCurve: Curves.easeOutSine,
    ));
  }

  @override
  void dispose() {
    _slidingAnimationCtrl.dispose();
    super.dispose();
  }

  void _slide(double delta) {
    if (delta < 0) {
      _slidingAnimationCtrl.forward();
    } else if (delta > 0) {
      _slidingAnimationCtrl.reverse();
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        if (_slidingAnimationCtrl.isDismissed) _slidingAnimationCtrl.forward();
      },
      onVerticalDragUpdate: (drag) {
        _slide(drag.delta.dy);
      },
      child: Container(
        color: Colors.transparent,
        child: Stack(
          children: [
            AnimatedBuilder(
              animation: _slidingAnim,
              builder: (context, child) {
                bottom = _slidingAnim.value;
                return Positioned(
                  bottom: 0,
                  child: Transform.translate(
                    offset: Offset(0, bottom),
                    child: Container(
                      decoration: BoxDecoration(
                          color: _backgroundColor,
                          borderRadius: BorderRadius.only(
                              topLeft: ThemeRadius.rad16,
                              topRight: ThemeRadius.rad16)),
                      height: widget.height,
                      width: size.width,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              MaterialButton(
                                  elevation: 4,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: ThemeRadius.all32),
                                  height: 48,
                                  color: themeColor.primary,
                                  textColor: themeColor.backgroundAccent,
                                  child: Text('Login'),
                                  onPressed: () {}),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  // );
                  // }
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
