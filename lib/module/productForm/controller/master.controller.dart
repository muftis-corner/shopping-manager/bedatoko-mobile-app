import 'package:bedatoko/api/service.dart';
import 'package:bedatoko/constant/config.dart';
import 'package:bedatoko/model/api/body.dart';
import 'package:bedatoko/model/master.dart';
import 'package:bedatoko/model/product/brand.dart';
import 'package:bedatoko/model/product/category.dart';
import 'package:dio/dio.dart';

class MasterController {
  static Future<List<Master>> getMasters(String apiName, int page, int limit,
      [String query]) async {
    try {
      final Response response = await API.post(
          API.URL + apiName + '/read', Body.read(page, limit, query));
      final data = response.data;
      if (data["status"]) {
        switch (apiName) {
          case ApiName.category:
            {
              return Category.createList(data["data"]);
            }
          case ApiName.brand:
            {
              return Brand.createList(data["data"]);
            }
        }
        throw "API Name not found";
      } else {
        throw "Data not found";
      }
    } catch (e) {
      print(e);
      throw "$e";
    }
  }

  static Future<Master> getMaster(String apiName, int id) async {
    try {
      final Response response =
          await API.post(API.URL + apiName + '/read', {"id": id});
      final data = response.data;

      if (data["status"]) {
        switch (apiName) {
          case ApiName.category:
            {
              return Category.create(data["data"]);
            }
          case ApiName.brand:
            {
              return Brand.create(data["data"]);
            }

            throw "API Name not found";
        }
      }

      throw "Data not found";
    } catch (e) {
      throw "$e";
    }
  }
}
