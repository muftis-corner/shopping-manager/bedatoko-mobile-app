import 'dart:async';

import 'package:bedatoko/constant/theme/theme.dart';
import 'package:bedatoko/model/master.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SearchListDialog extends StatefulWidget {
  const SearchListDialog({Key key, this.title, this.future}) : super(key: key);

  final String title;
  final Future<List<Master>> future;

  @override
  _SearchListDialogState createState() => _SearchListDialogState();
}

class _SearchListDialogState extends State<SearchListDialog> {
  TextEditingController textController;
  StreamController streamController = StreamController<String>();

  @override
  void dispose() {
    streamController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(32.0),
      child: Scaffold(
        appBar: AppBar(
          title: Text('${widget.title}'),
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: CupertinoTextField(
                controller: textController,
                onChanged: (text) {
                  streamController.add(text);
                },
                prefix: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(
                    FontAwesomeIcons.search,
                    color: themeColor.hint,
                    size: 12,
                  ),
                ),
                style: ThemeText.form,
              ),
            ),
            Expanded(
                child: FutureBuilder<List<Master>>(
              future: widget.future,
              builder: (context, fSnapshot) {
                if (fSnapshot.hasData) {
                  fSnapshot.data.sort((a, b) => a.name.compareTo(b.name));
                  return StreamBuilder<String>(
                      stream: streamController.stream,
                      builder: (context, sSnapshot) {
                        if (sSnapshot.hasData) {
                          return CupertinoScrollbar(
                            child: ListView(
                                children: fSnapshot.data
                                    .where((data) => data.name
                                        .toLowerCase()
                                        .contains(sSnapshot.data.toLowerCase()))
                                    .map<Widget>(
                                        (item) => DialogListItem(item: item))
                                    .toList()),
                          );
                        } else {
                          return ListView(
                              children: fSnapshot.data
                                  .map<Widget>((item) => DialogListItem(
                                        item: item,
                                      ))
                                  .toList());
                        }
                      });
                } else {
                  return Center(
                    child: CircularProgressIndicator(
                      backgroundColor: Colors.black,
                    ),
                  );
                }
              },
            )),
          ],
        ),
      ),
    );
  }
}

class DialogListItem extends StatelessWidget {
  final Master item;
  const DialogListItem({
    Key key,
    this.item,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context, item);
      },
      child: Container(
        color: Colors.transparent,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 16.0, top: 8.0),
              child: Text(
                item.name,
                style: ThemeText.theme.subtitle2,
              ),
            ),
            Divider(
              thickness: 0.6,
              color: themeColor.primaryAccent.withOpacity(0.5),
            ),
          ],
        ),
      ),
    );
  }
}
