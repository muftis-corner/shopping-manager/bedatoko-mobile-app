import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:bedatoko/constant/theme/theme.dart';

class TextForm extends StatefulWidget {
  final TextEditingController controller;
  final String title;
  final bool readOnly;
  final Function onTap;
  final ValueChanged<String> onChanged;

  const TextForm(
      {Key key,
      this.controller,
      this.title,
      this.readOnly = false,
      this.onTap,
      this.onChanged})
      : super(key: key);

  @override
  _FormState createState() => _FormState();
}

class _FormState extends State<TextForm> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        widget.title ?? "",
        style: ThemeText.theme.headline6
            .copyWith(fontSize: 14, fontWeight: FontWeight.bold),
      ),
      subtitle: Padding(
        padding: const EdgeInsets.only(top: 4.0),
        child: CupertinoTextField(
          controller: widget.controller,
          style: ThemeText.form,
          readOnly: widget.readOnly,
          onTap: widget.onTap,
          onChanged: widget.onChanged,
        ),
      ),
    );
  }
}

class TextFormWithDropdown extends StatelessWidget {
  final TextEditingController controller;
  final String title;
  final TextInputType keyboardType;
  final Widget dropdownButton;
  final ValueChanged<String> onChanged;

  const TextFormWithDropdown({
    Key key,
    this.controller,
    this.title,
    this.keyboardType,
    this.dropdownButton,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        this.title ?? "",
        style: ThemeText.theme.headline6
            .copyWith(fontSize: 14, fontWeight: FontWeight.bold),
      ),
      subtitle: Row(
        children: [
          Expanded(
            flex: 5,
            child: Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: CupertinoTextField(
                textAlign: TextAlign.end,
                controller: this.controller,
                onChanged: this.onChanged,
                keyboardType: this.keyboardType,
                style: ThemeText.form,
              ),
            ),
          ),
          Expanded(flex: 2, child: this.dropdownButton)
        ],
      ),
    );
  }
}

class DropdownButtonForm<T> extends StatelessWidget {
  final T value;
  final List<DropdownMenuItem<T>> items;
  final ValueChanged onChanged;

  DropdownButtonForm({
    Key key,
    this.value,
    this.items,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField(
      decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent))),
      value: this.value,
      items: this.items,
      onChanged: onChanged,
    );
  }
}
