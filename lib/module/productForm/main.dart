import 'dart:async';

import 'package:bedatoko/constant/config.dart';
import 'package:bedatoko/model/product/brand.dart';
import 'package:bedatoko/model/product/product.dart';
import 'package:bedatoko/module/camera/camera.dart';
import 'package:bedatoko/module/camera/model/imageData.dart';
import 'package:bedatoko/module/productForm/widgets/searchListDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:bedatoko/constant/theme/theme.dart';
import 'package:bedatoko/model/master.dart';
import 'package:bedatoko/model/product/category.dart';
import 'package:bedatoko/model/product/net.dart';
import 'package:bedatoko/module/productForm/controller/master.controller.dart';
import 'package:bedatoko/module/productForm/widgets/form.dart';

class ProductForm extends StatefulWidget {
  final String barcode;

  const ProductForm({Key key, this.barcode}) : super(key: key);
  @override
  _ProductFormState createState() => _ProductFormState();
}

class _ProductFormState extends State<ProductForm> {
  Product temp = Product();

  TextEditingController name = TextEditingController();
  TextEditingController category = TextEditingController();
  TextEditingController brand = TextEditingController();
  TextEditingController barcode = TextEditingController();
  TextEditingController detail = TextEditingController();
  TextEditingController quantity = TextEditingController();
  ImageData image;

  @override
  void initState() {
    temp.net = Net.data[0];
    super.initState();
  }

  @override
  void dispose() {
    name.dispose();
    barcode.dispose();
    detail.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add New Product'),
      ),
      body: ListView(
        children: [
          if (widget.barcode != null)
            ListTile(
                title: Text(
                  "Barcode",
                  style: ThemeText.theme.headline6
                      .copyWith(fontSize: 14, fontWeight: FontWeight.bold),
                ),
                subtitle: Text(
                  '${widget.barcode}',
                  style: ThemeText.theme.bodyText1,
                ))
          else
            TextForm(
              title: 'Barcode',
              controller: barcode,
            ),
          TextForm(
            title: "Category",
            controller: category,
            readOnly: true,
            onTap: () async {
              temp.category = await showDialog(
                context: context,
                builder: (context) => SearchListDialog(
                  title: "Category",
                  future: MasterController.getMasters(ApiName.category, 0, 999),
                ),
              );
              category.text = temp.category.name;
            },
          ),
          TextForm(
            title: "Product's Name",
            controller: name,
            onChanged: (text) {
              temp.name = text;
            },
          ),
          TextForm(
            title: "Brand",
            onTap: () async {
              temp.brand = await showDialog(
                context: context,
                builder: (context) => SearchListDialog(
                    title: "Brand",
                    future: MasterController.getMasters(ApiName.brand, 0, 999)),
              );
              brand.text = temp.brand.name;
            },
            controller: brand,
            readOnly: true,
          ),
          TextFormWithDropdown(
            title: "Quantity",
            controller: quantity,
            onChanged: (text) {
              temp.size = double.parse(text);
            },
            keyboardType: TextInputType.number,
            dropdownButton: DropdownButtonForm<Net>(
              items: Net.data
                  .map((e) => DropdownMenuItem(
                        child: Text('${e.name}'),
                        value: e,
                      ))
                  .toList(),
              value: temp.net,
              onChanged: (value) {
                setState(() {
                  this.temp.net = value;
                });
              },
            ),
          ),
          ListTile(
            title: Text(
              "Image",
              style: ThemeText.theme.headline6
                  .copyWith(fontSize: 14, fontWeight: FontWeight.bold),
            ),
            subtitle: Center(
              child: GestureDetector(
                  onTap: () async {
                    ImageData _imageData = await Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                CameraScreen(camera: Config.cameras)));
                    setState(() {
                      if (_imageData != null) {
                        this.image = _imageData;
                      }
                    });
                  },
                  child: this.image == null
                      ? Container(
                          margin: EdgeInsets.only(top: 16),
                          width: 300,
                          height: 200,
                          decoration: BoxDecoration(
                              border:
                                  Border.all(color: themeColor.hint, width: 1),
                              borderRadius: BorderRadius.circular(8)),
                          child: Icon(
                            FontAwesomeIcons.cameraRetro,
                            size: 100,
                            color: themeColor.hint,
                          ))
                      : Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: AspectRatio(
                            aspectRatio: 1,
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  image: DecorationImage(
                                      image: this.image.imageProvider,
                                      fit: BoxFit.cover)),
                            ),
                          ),
                        )),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
            child: MaterialButton(
                color: themeColor.secondary,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8)),
                minWidth: 200,
                height: 40,
                textColor: themeColor.backgroundAccent,
                child: Text(
                  'Save New Product',
                  style: ThemeText.buttonM,
                ),
                onPressed: () {}),
          )
        ],
      ),
    );
  }
}
