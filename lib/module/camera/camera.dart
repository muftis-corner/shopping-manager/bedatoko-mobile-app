import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' as join;
import 'package:path_provider/path_provider.dart';

import 'model/imageData.dart';

class CameraScreen extends StatefulWidget {
  final List<CameraDescription> camera;

  const CameraScreen({
    Key key,
    @required this.camera,
  }) : super(key: key);

  @override
  CameraScreenState createState() => CameraScreenState();
}

class CameraScreenState extends State<CameraScreen> {
  CameraController _controller;
  Future<void> _initializeControllerFuture;
  double aspectRatio;
  CameraDescription _camera;

  void takePicture() async {
    try {
      await _initializeControllerFuture;

      final path = join.join(
        (await getTemporaryDirectory()).path,
        '${DateTime.now()}.png',
      );

      await _controller.takePicture(path);

      File image = File(path);

      var decodedImage = await decodeImageFromList(image.readAsBytesSync());

      int imageWidth = decodedImage.width;
      int imageHeight = decodedImage.height;

      Navigator.pop(
          context,
          ImageData(
              path: path,
              height: imageHeight,
              width: imageWidth,
              dateTaken: DateTime.now()));
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
    _camera = widget.camera[0];

    _controller =
        CameraController(_camera, ResolutionPreset.veryHigh, enableAudio: true);
    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.black,
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Center(
              child: FutureBuilder<void>(
                future: _initializeControllerFuture,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    return AspectRatio(
                      aspectRatio: 1080 / 1920,
                      child: CameraPreview(_controller),
                    );
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                },
              ),
            ),
            ColorFiltered(
              colorFilter: ColorFilter.mode(
                  Colors.black.withOpacity(1.0), BlendMode.srcOut),
              child: Stack(
                fit: StackFit.expand,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.black,
                        backgroundBlendMode: BlendMode.dstOut),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                      // margin: const EdgeInsets.only(top: 80),
                      height: size.width,
                      width: size.width,
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: BorderRadius.circular(8),
                      ),
                    ),
                  )
                ],
              ),
            ),
            // Center(
            //     child: Image.asset(
            //   'assets/absensi/foto.png',
            //   width: 220,
            // )),
            Positioned(
              bottom: 30,
              left: 0,
              right: 0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Icon(Icons.arrow_back),
                      shape: CircleBorder(),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: RaisedButton(
                      onPressed: () {
                        takePicture();
                      },
                      shape: CircleBorder(),
                      child: Container(
                        height: 64,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

// A widget that displays the picture taken by the user.
class DisplayPictureScreen extends StatelessWidget {
  final String imagePath;

  const DisplayPictureScreen({Key key, this.imagePath}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          brightness: Brightness.dark, title: Text('Display the Picture')),
      // The image is stored as a file on the device. Use the `Image.file`
      // constructor with the given path to display the image.
      body: Image.file(File(imagePath)),
    );
  }
}
