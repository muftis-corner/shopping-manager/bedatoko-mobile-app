import 'dart:io';

import 'package:flutter/rendering.dart';
import 'package:image_picker/image_picker.dart';

import 'model/imageData.dart';

class Gallery {
  static Future<ImageData> getImages() async {
    final data = await ImagePicker().getImage(source: ImageSource.gallery);
    final image = File(data.path);
    final decodedImage = await decodeImageFromList(image.readAsBytesSync());

    return ImageData(
        path: data.path,
        width: decodedImage.width,
        height: decodedImage.height,
        dateTaken: DateTime.now());
  }
}
