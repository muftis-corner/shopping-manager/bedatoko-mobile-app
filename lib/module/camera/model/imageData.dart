import 'dart:io';

import 'package:flutter/material.dart';

class ImageData {
  final String path;

  final int height;

  final int width;

  final DateTime dateTaken;

  ImageData({this.path, this.height, this.width, this.dateTaken});

  ///[get Image Height]
  num get getHeight => this.height;

  ///[get Image Width]
  num get getWidth => this.width;

  ///[get Image as File]
  File get file => File(this.path);

  ///[get Image as ImageProvider]
  ImageProvider get imageProvider => FileImage(this.file);

  @override
  String toString() {
    return path;
  }
}
