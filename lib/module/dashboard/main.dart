import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:bedatoko/constant/menu/bottomNavbar.dart';
import 'package:bedatoko/constant/theme/textStyle.dart';
import 'package:bedatoko/constant/theme/theme.dart';
import 'package:bedatoko/module/dashboard/page/home/home.dart';
import 'package:bedatoko/module/dashboard/page/myItem/main.dart';

import 'package:bedatoko/module/productForm/productForm.dart';
import 'package:bedatoko/module/scanner/scanner.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'controller/dash.controller.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> with TickerProviderStateMixin {
  Size size;
  int currentIndex = 0;
  String title = "Home";
  AnimationController btmBarController;

  Widget page() {
    switch (currentIndex) {
      case 0:
        return HomePage();
        break;
      case 1:
        return HomePage();
        break;
      case 3:
        return MyItemPage();
        break;
      case 4:
        return HomePage();
        break;
      default:
        return null;
    }
  }

  @override
  void initState() {
    btmBarController = AnimationController(
        duration: Duration(milliseconds: 200),
        reverseDuration: Duration(milliseconds: 200),
        vsync: this);
    DashController.getRecentProductDash();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;

    return Scaffold(
        body: page(),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: currentIndex != 2
            ? FloatingActionButton(
                backgroundColor: themeColor.secondary,
                onPressed: () async {
                  String barcode = (await BarcodeScanner.scan()) ?? 'empty';
                  if (barcode != '-1') {
                    return AwesomeDialog(
                      context: context,
                      dialogType: DialogType.WARNING,
                      headerAnimationLoop: false,
                      animType: AnimType.BOTTOMSLIDE,
                      title: 'Product Not Found',
                      desc: "add new product ?",
                      showCloseIcon: true,
                      btnCancelOnPress: () {},
                      btnOkOnPress: () {
                        return Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ProductForm(
                              barcode: barcode,
                            ),
                          ),
                        );
                      },
                    )..show();
                  }
                },
                child: Icon(FontAwesomeIcons.barcode,
                    color: themeColor.backgroundAccent),
              )
            : null,
        bottomNavigationBar: BottomAppBar(
          elevation: 2,
          shape: CircularNotchedRectangle(),
          color: themeColor.primary,
          child: Container(
            height: 60,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: dashboardNavbarMenuItem
                  .map((e) => GestureDetector(
                      child: Container(
                        width: size.width / 5,
                        color: Colors.transparent,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(
                              e.icons,
                              color: themeColor.backgroundAccent,
                            ),
                            if (currentIndex ==
                                dashboardNavbarMenuItem.indexOf(e))
                              Text(
                                e.title,
                                style: ThemeText.titleS.copyWith(fontSize: 12),
                              )
                          ],
                        ),
                      ),
                      onTap: () {
                        if (dashboardNavbarMenuItem.indexOf(e) != 2)
                          setState(() {
                            {
                              currentIndex = dashboardNavbarMenuItem.indexOf(e);
                            }
                          });
                      }))
                  .toList(),
            ),
          ),
        )
        //   BottomNavigationBar(

        //   backgroundColor: themeColor.primary,
        //   currentIndex: currentIndex,
        //   onTap: (index) {
        //     setState(() {
        //       currentIndex = index;
        //       this.title = dashboardNavbarMenuItem[index].title;
        //     });
        //   },
        //   selectedItemColor: Colors.black,
        //   selectedIconTheme: IconThemeData(color: Colors.black),
        //   selectedLabelStyle: TextStyle(color: Colors.black),
        //   unselectedLabelStyle: TextStyle(color: Colors.grey),
        //   showUnselectedLabels: true,
        //   showSelectedLabels: true,
        //   type: BottomNavigationBarType.fixed,
        //   items: dashboardNavbarMenuItem
        //       .map((e) => BottomNavigationBarItem(icon: e.icons, label: e.title))
        //       .toList(),
        // ),
        );
  }
}
