import 'dart:convert';

import 'package:flutter/material.dart';

class MenuItem {
  final String title;
  final String description;
  final IconData icons;
  final String route;
  MenuItem({
    this.title,
    this.description,
    this.icons,
    this.route,
  });

  MenuItem copyWith({
    String title,
    String description,
    IconData icons,
    String route,
  }) {
    return MenuItem(
      title: title ?? this.title,
      description: description ?? this.description,
      icons: icons ?? this.icons,
      route: route ?? this.route,
    );
  }

  @override
  String toString() {
    return 'MenuItem(title: $title, description: $description, icons: $icons, route: $route)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is MenuItem &&
        o.title == title &&
        o.description == description &&
        o.icons == icons &&
        o.route == route;
  }

  @override
  int get hashCode {
    return title.hashCode ^
        description.hashCode ^
        icons.hashCode ^
        route.hashCode;
  }
}
