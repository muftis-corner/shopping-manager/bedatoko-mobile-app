import 'package:bedatoko/api/service.dart';
import 'package:bedatoko/model/api/body.dart';
import 'package:bedatoko/model/product/product.dart';
import 'package:dio/dio.dart';

class DashController {
  static Future checkServer() async {
    final Response response = await API.get(API.URL);
    final data = response.data;
    print(data);
  }

  static Future getRecentProductDash() async {
    try {
      final Response response =
          await API.post(API.URL + '/product/read', Body.read(0, 10));
      final data = response.data;
      if (data['status']) {
        return Product.create(response);
      } else {
        throw false;
      }
    } catch (e) {
      print("error : $e");
    }
  }
}
