import 'dart:async';

import 'package:bedatoko/constant/format.dart';
import 'package:bedatoko/module/dashboard/page/myItem/widget/homeList.dart';
import 'package:bedatoko/module/dashboard/page/myItem/widget/listTitle.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

import 'package:bedatoko/constant/theme/icon.dart';
import 'package:bedatoko/constant/theme/radius.dart';
import 'package:bedatoko/constant/theme/theme.dart';
import 'package:bedatoko/model/shop/shop.dart';
import 'package:bedatoko/widgets/recordProduct/recordProduct.dart';
import 'package:bedatoko/widgets/slider/slider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Size size;

  double _bgSearchOpacity;
  TextEditingController search = TextEditingController();
  ScrollController scroll = ScrollController();
  StreamController scrollStreamController = StreamController<double>();
  Stream scrollStream;

  Stream scrollOffsetStream() async* {}

  @override
  void initState() {
    _bgSearchOpacity = 0;
    scrollStream = scrollStreamController.stream;
    scrollStreamController.add(0.0);
    scroll.addListener(() {
      if (scroll.offset <= 139) {
        scrollStreamController.add(scroll.offset);
      } else if (_bgSearchOpacity != 1.0) {
        scrollStreamController.add(140.0);
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    search.dispose();
    super.dispose();
  }

  Widget searchBar() {
    return Padding(
      padding:
          const EdgeInsets.only(left: 8.0, right: 8.0, top: 32.0, bottom: 8.0),
      child: CupertinoTextField(
        decoration: BoxDecoration(
            color: themeColor.backgroundAccent.withOpacity(0.9),
            border: Border.all(color: themeColor.primary, width: 2),
            borderRadius: ThemeRadius.searchBar),
        placeholder: 'Find Your Item',
        placeholderStyle: ThemeText.hint,
        prefix: Padding(
          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
          child: Icon(
            ThemeIcon.search,
            size: 20,
            color: themeColor.text,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
    size = MediaQuery.of(context).size;
    return Stack(
      children: [
        CustomScrollView(
          controller: scroll,
          slivers: [
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.only(top: 72.0),
                child: SliderPage(),
              ),
            ),
            SliverToBoxAdapter(
              child: ListTitle(
                title: 'Recent Product',
                showMore: () {},
              ),
            ),
            SliverToBoxAdapter(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 200,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: [
                        ListHorizontalProduct(),
                        ListHorizontalProduct(),
                        ListHorizontalProduct(),
                        ListHorizontalProduct(),
                      ],
                    ),
                  ),
                  Divider(
                    thickness: 0.5,
                    color: themeColor.primary.withOpacity(0.3),
                  )
                ],
              ),
            ),
            SliverToBoxAdapter(
              child: ListTitle(
                title: 'All Product',
                showMore: () {},
              ),
            ),
            SliverToBoxAdapter(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 200,
                    child: ListView.builder(
                      itemCount: 5,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        return ListHorizontalProduct();
                      },
                    ),
                  ),
                  Divider(
                    thickness: 0.5,
                    color: themeColor.primary.withOpacity(0.3),
                  ),
                ],
              ),
            ),
            SliverToBoxAdapter(
              child: ListTitle(
                title: 'Recent List',
                showMore: () {},
              ),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) {
                return HomeList(
                  shoppingDate: DateTime.now(),
                  shop: Shop(id: 0, name: 'Lotte Grosir'),
                  total: 120000,
                );
              }, childCount: 5),
            ),
            SliverToBoxAdapter(
              child: SizedBox(
                height: 30,
              ),
            )
          ],
        ),
        Positioned(
            top: 0,
            child: StreamBuilder<double>(
                stream: scrollStream,
                builder: (context, snapshot) {
                  print(snapshot.data);
                  _bgSearchOpacity = (snapshot.data ?? 0.1) / 140;
                  if (_bgSearchOpacity >= 0.5) {
                    SystemChrome.setSystemUIOverlayStyle(
                        SystemUiOverlayStyle.light);
                  } else {
                    SystemChrome.setSystemUIOverlayStyle(
                        SystemUiOverlayStyle.dark);
                  }
                  return Container(
                      height: 76,
                      width: size.width,
                      color: themeColor.primary.withOpacity(_bgSearchOpacity),
                      child: searchBar());
                })),
      ],
    );
  }
}
