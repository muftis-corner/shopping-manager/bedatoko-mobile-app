import 'dart:async';

import 'package:bedatoko/constant/format.dart';
import 'package:bedatoko/constant/theme/icon.dart';
import 'package:bedatoko/constant/theme/radius.dart';
import 'package:bedatoko/constant/theme/theme.dart';
import 'package:bedatoko/model/product/category.dart';
import 'package:bedatoko/model/product/product.dart';
import 'package:bedatoko/model/product/net.dart';
import 'package:bedatoko/model/record/record.dart';
import 'package:bedatoko/model/shop/shop.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:latlng/latlng.dart';

class MyItemPage extends StatefulWidget {
  @override
  _MyItemPageState createState() => _MyItemPageState();
}

class _MyItemPageState extends State<MyItemPage> with TickerProviderStateMixin {
  AnimationController totalController;
  Size size;

  @override
  void initState() {
    totalController = AnimationController(
        duration: Duration(milliseconds: 300),
        reverseDuration: Duration(milliseconds: 300),
        vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    this.size = MediaQuery.of(context).size;

    return Stack(
      children: [
        Positioned(
          top: 0,
          left: 0,
          child: Container(
            height: 100,
            width: size.width,
            color: themeColor.primary,
            child: Align(
              child: Padding(
                padding: const EdgeInsets.only(left: 16.0, bottom: 36),
                child: Row(
                  children: [
                    Icon(
                      FontAwesomeIcons.shoppingCart,
                      color: themeColor.title,
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Text(
                      'My List',
                      style: ThemeText.titleM,
                    ),
                  ],
                ),
              ),
              alignment: Alignment.bottomLeft,
            ),
          ),
        ),
        Positioned(
          top: 80,
          child: Container(
              decoration: BoxDecoration(
                  color: themeColor.background,
                  borderRadius: BorderRadius.only(topRight: ThemeRadius.rad20)),
              height: size.height - 80,
              width: size.width,
              child: Padding(
                padding: const EdgeInsets.only(top: 50.0),
                child: ListView.builder(itemBuilder: (context, index) {
                  return MyListItem(
                    item: _sample,
                  );
                }),
              )),
        ),
        Positioned(
          top: 88,
          left: 8,
          right: 8,
          child: Row(
            children: [
              Expanded(
                flex: 7,
                child: CupertinoTextField(
                  decoration: BoxDecoration(
                    color: themeColor.backgroundAccent.withOpacity(0.9),
                    border: Border.all(color: themeColor.primary, width: 1.2),
                  ),
                  placeholder: 'Find Your Item',
                  placeholderStyle: ThemeText.hint,
                  prefix: Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: Icon(
                      ThemeIcon.search,
                      size: 20,
                      color: themeColor.text,
                    ),
                  ),
                ),
              ),
              SizedBox(width: 8),
              Expanded(
                flex: 2,
                child: Container(
                  decoration: BoxDecoration(
                    color: themeColor.primary,
                    borderRadius: ThemeRadius
                        .borderRight20, // border: Border.all(color: themeColor.primary, width: 1.2),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Row(
                      children: [
                        Icon(Icons.filter_list_outlined,
                            color: themeColor.backgroundAccent),
                        SizedBox(
                          width: 4,
                        ),
                        Text('Filter',
                            style: ThemeText.text
                                .copyWith(color: themeColor.backgroundAccent))
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        Positioned(bottom: 30, right: 8, child: floatingTotal())
      ],
    );
  }

  GestureDetector floatingTotal() {
    return GestureDetector(
      onTapDown: (value) {
        totalController.reverse();
      },
      onTapCancel: () {
        Future.delayed(Duration(seconds: 2), () {
          totalController.forward();
        });
      },
      onTapUp: (value) {
        Future.delayed(Duration(seconds: 2), () {
          totalController.forward();
        });
      },
      child: AnimatedBuilder(
          animation: totalController,
          builder: (context, child) {
            return Container(
              padding: EdgeInsets.only(top: 4, bottom: 4, left: 24, right: 16),
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        offset: Offset(0, 4),
                        blurRadius: 4,
                        color: themeColor.primary
                            .withOpacity(totalController.value ?? 0.0))
                  ],
                  borderRadius: ThemeRadius.borderLeft32,
                  color: themeColor.primary.withOpacity(totalController.value)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Total :',
                    style: ThemeText.text.copyWith(
                        fontSize: 12,
                        color: themeColor.backgroundAccent
                            .withOpacity(totalController.value)),
                  ),
                  Text('(4) Rp 32.000',
                      style: ThemeText.titleS.copyWith(
                          color: themeColor.backgroundAccent
                              .withOpacity(totalController.value))),
                ],
              ),
            );
          }),
    );
  }
}

Record _sample = Record(
    id: 0,
    createdDate: DateTime.now(),
    price: 20000,
    shop: Shop(
      name: 'Lotte Grosir Palembang',
      latLng: LatLng(-2.9490619, 104.761303),
      id: 0,
    ),
    productIds: [1, 1, 1, 1]
    //  Product(
    //     id: 0,
    //     barcode: '20023874283',
    //     category: Category(
    //       id: 0,
    //       name: 'beverage',
    //     ),
    //     imageUrl:
    //         'https://cf.shopee.co.id/file/46ec0a15ccb3d0a611a43acf8a6ca112',
    //     name: 'Susu Ultramilk 1000 ml',
    //     net: Net.data[3],
    //     size: 1000)
    );

class MyListItem extends StatefulWidget {
  final Record item;

  const MyListItem({
    Key key,
    this.item,
  }) : super(key: key);

  @override
  _MyListItemState createState() => _MyListItemState();
}

class _MyListItemState extends State<MyListItem> {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Row(
        children: [
          Container(
              margin: EdgeInsets.all(8),
              height: 68,
              width: 68,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage('${widget.item.productIds}')))),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(right: 8),
              height: 76,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('${widget.item.productIds}',
                      style: ThemeText.titleS
                          .copyWith(color: themeColor.primary, fontSize: 14)),
                  Row(
                    children: [
                      Icon(
                        Icons.location_on_outlined,
                        size: 12,
                        color: themeColor.text,
                      ),
                      Text('${widget.item.shop.name}',
                          style: ThemeText.text.copyWith(fontSize: 12)),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Container(
                            height: 24,
                            width: 24,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: themeColor.background,
                                border: Border.all(
                                    color: themeColor.primary, width: 1)),
                            child: Center(
                              child: Align(
                                alignment: Alignment.center,
                                child: Text(
                                  '-',
                                  style: ThemeText.buttonS
                                      .copyWith(color: themeColor.primary),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 8.0),
                            child: Text(
                              '2',
                              style: ThemeText.titleS.copyWith(
                                  color: themeColor.primary,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                          Container(
                            height: 24,
                            width: 24,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: themeColor.primary,
                            ),
                            child: Center(
                              child: Align(
                                alignment: Alignment.center,
                                child: Text(
                                  '+',
                                  style: ThemeText.buttonS
                                      .copyWith(color: themeColor.background),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Text(
                        rp.format(widget.item.price),
                        style: ThemeText.titleS
                            .copyWith(color: themeColor.secondary),
                      )
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
