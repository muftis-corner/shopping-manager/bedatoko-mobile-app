import 'package:bedatoko/constant/theme/radius.dart';
import 'package:bedatoko/constant/theme/theme.dart';
import 'package:flutter/material.dart';

class ListTitle extends StatelessWidget {
  final String title;
  final Function showMore;
  const ListTitle({
    Key key,
    this.title,
    this.showMore,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          margin: EdgeInsets.only(top: 8),
          padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 3),
          decoration: BoxDecoration(
              color: themeColor.primary,
              borderRadius: ThemeRadius.borderRight20),
          child: Text(
            '$title',
            style: ThemeText.titleS,
          ),
        ),
        FlatButton(
            shape: RoundedRectangleBorder(),
            onPressed: showMore,
            child: Text(
              'Show More >',
              style: ThemeText.titleS.copyWith(color: themeColor.secondary),
            ))
      ],
    );
  }
}
