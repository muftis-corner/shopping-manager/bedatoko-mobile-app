import 'package:bedatoko/constant/format.dart';
import 'package:bedatoko/constant/theme/theme.dart';
import 'package:bedatoko/model/shop/shop.dart';
import 'package:flutter/material.dart';

class HomeList extends StatelessWidget {
  final DateTime shoppingDate;
  final Shop shop;
  final double total;

  const HomeList({
    Key key,
    this.shoppingDate,
    this.shop,
    this.total,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
      child: Padding(
        padding: const EdgeInsets.all(4.0),
        child: Row(
          children: [
            Expanded(
                flex: 15,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 2.0),
                      child: Text(
                        '${shoppingDate.toString()}',
                        style: ThemeText.titleS
                            .copyWith(color: themeColor.text, fontSize: 14),
                      ),
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.location_on_outlined,
                          size: 12,
                          color: themeColor.text,
                        ),
                        Text('Lotte Grosir',
                            style: ThemeText.text.copyWith(fontSize: 12)),
                      ],
                    ),
                  ],
                )),
            Expanded(
                flex: 6,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Total Belanja :',
                        style: ThemeText.text.copyWith(fontSize: 12)),
                    Text(
                      '${rp.format(total)}',
                      style: ThemeText.titleS
                          .copyWith(color: themeColor.secondary, fontSize: 14),
                    ),
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
