class Body {
  static Map<String, dynamic> read(int page, int limit, [String query]) {
    return {'limit': limit, 'page': page, 'query': query};
  }

  static Map<String, dynamic> delete(int id) {
    return {'id': id};
  }
}
