import 'package:dio/dio.dart';

class ApiResponse {
  Response _response;

  ApiResponse(this._response);

  String get message => _response.data['message'];
  bool get status => _response.data['status'];
  dynamic get data => _response.data['data'][0];

  @override
  String toString() {
    return '$message, $status, $data';
  }
}
