abstract class Master {
  String name;
  int id;

  Master(
    this.id,
    this.name,
  );

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Master && o.name == name && o.id == id;
  }

  @override
  int get hashCode => name.hashCode ^ id.hashCode;
}
