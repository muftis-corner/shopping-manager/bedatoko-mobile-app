import 'dart:convert';

import 'package:bedatoko/model/master.dart';

class Brand extends Master {
  Brand({
    int id,
    String name,
  }) : super(id, name);

  Brand copyWith({
    int id,
    String name,
  }) {
    return Brand(
      id: id ?? this.id,
      name: name ?? this.name,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }

  factory Brand.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Brand(
      id: map['id'],
      name: map['name'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Brand.fromJson(String source) => Brand.fromMap(json.decode(source));

  @override
  String toString() => 'Brand(id: $id, name: $name)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Brand && o.id == id && o.name == name;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode;

  static Brand create(data) => Brand(id: data["id"], name: data["brand"]);

  static List<Brand> createList(data) =>
      data.map<Brand>((item) => Brand.create(item)).toList();
}
