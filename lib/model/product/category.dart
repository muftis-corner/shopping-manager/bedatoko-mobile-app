import 'dart:convert';

import 'package:bedatoko/model/master.dart';
import 'package:flutter/foundation.dart';

class Category extends Master {
  Category({
    int id,
    String name,
  }) : super(id, name);

  static Category create(Map data) {
    return Category(
      id: data["id"],
      name: data["category"],
    );
  }

  static List<Category> createList(List data) {
    return data.map((item) => Category.create(item)).toList();
  }

  Category copyWith({
    int id,
    String name,
  }) {
    return Category(
      id: id ?? this.id,
      name: name ?? this.name,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }

  factory Category.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Category(
      id: map['id'],
      name: map['name'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Category.fromJson(String source) =>
      Category.fromMap(json.decode(source));

  @override
  String toString() => 'Category(id: $id, name: $name)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Category && o.id == id && o.name == name;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode;
}
