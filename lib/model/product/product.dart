import 'dart:convert';

import 'package:bedatoko/data/session.dart';

import 'brand.dart';
import 'category.dart';
import 'net.dart';

class Product {
  int id;
  String name;
  String barcode;
  String imageUrl;
  Category category;
  Brand brand;
  double size;
  Net net;

  Product({
    this.id,
    this.name,
    this.barcode,
    this.imageUrl,
    this.category,
    this.brand,
    this.size,
    this.net,
  });

  Product copyWith({
    int id,
    String name,
    String barcode,
    String imageUrl,
    Category category,
    Brand brand,
    double size,
    Net net,
  }) {
    return Product(
      id: id ?? this.id,
      name: name ?? this.name,
      barcode: barcode ?? this.barcode,
      imageUrl: imageUrl ?? this.imageUrl,
      category: category ?? this.category,
      brand: brand ?? this.brand,
      size: size ?? this.size,
      net: net ?? this.net,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'barcode': barcode,
      'imageUrl': imageUrl,
      'category': category?.toMap(),
      'brand': brand?.toMap(),
      'size': size,
      'net': net?.toMap(),
    };
  }

  factory Product.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Product(
      id: map['id'],
      name: map['name'],
      barcode: map['barcode'],
      imageUrl: map['imageUrl'],
      category: Category.fromMap(map['category']),
      brand: Brand.fromMap(map['brand']),
      size: map['size'],
      net: Net.fromMap(map['net']),
    );
  }

  String toJson() => json.encode(toMap());

  factory Product.fromJson(String source) =>
      Product.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Product(id: $id, name: $name, barcode: $barcode, imageUrl: $imageUrl, category: $category, brand: $brand, size: $size, net: $net)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Product &&
        o.id == id &&
        o.name == name &&
        o.barcode == barcode &&
        o.imageUrl == imageUrl &&
        o.category == category &&
        o.brand == brand &&
        o.size == size &&
        o.net == net;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        name.hashCode ^
        barcode.hashCode ^
        imageUrl.hashCode ^
        category.hashCode ^
        brand.hashCode ^
        size.hashCode ^
        net.hashCode;
  }

  Map body() => {
        'barcode': this.barcode,
        'productName': this.name,
        'netValue': this.size,
        'imgUrl': this.imageUrl,
        'idCategory': this.category.id,
        'idBrand': this.brand.id,
        'idNet': this.net.id,
        'createdBy': Session.userId
      };

  static Product create(dynamic value) => Product();
}
