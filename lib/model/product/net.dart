import 'dart:convert';

class Net {
  int id;
  String name;
  Net({
    this.id,
    this.name,
  });

  static List<String> _data = [
    'Kg',
    'Gr',
    'Ltr',
    'ml',
    'pack',
    'item',
    'net',
  ];

  static List<Net> data =
      _data.map((e) => Net(id: _data.indexOf(e), name: e)).toList();

  Net copyWith({
    int id,
    String name,
  }) {
    return Net(
      id: id ?? this.id,
      name: name ?? this.name,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }

  factory Net.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Net(
      id: map['id'],
      name: map['name'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Net.fromJson(String source) => Net.fromMap(json.decode(source));

  @override
  String toString() => 'Net(id: $id, name: $name)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Net && o.id == id && o.name == name;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode;
}
