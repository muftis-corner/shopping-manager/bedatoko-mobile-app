import 'dart:convert';

class StoreType {
  int id;
  String name;
  StoreType({
    this.id,
    this.name,
  });

  StoreType copyWith({
    int id,
    String name,
  }) {
    return StoreType(
      id: id ?? this.id,
      name: name ?? this.name,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
    };
  }

  factory StoreType.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return StoreType(
      id: map['id'],
      name: map['name'],
    );
  }

  String toJson() => json.encode(toMap());

  factory StoreType.fromJson(String source) =>
      StoreType.fromMap(json.decode(source));

  @override
  String toString() => 'StoreType(id: $id, name: $name)';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is StoreType && o.id == id && o.name == name;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode;
}
