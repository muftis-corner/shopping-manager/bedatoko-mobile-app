import 'dart:convert';

import 'package:bedatoko/model/shop/shopType.dart';
import 'package:latlng/latlng.dart';

class Shop {
  final int id;
  final String name;
  final StoreType storeType;
  final LatLng latLng;
  Shop({
    this.id,
    this.name,
    this.storeType,
    this.latLng,
  });

  Shop copyWith({
    int id,
    String name,
    StoreType storeType,
    LatLng latLng,
  }) {
    return Shop(
      id: id ?? this.id,
      name: name ?? this.name,
      storeType: storeType ?? this.storeType,
      latLng: latLng ?? this.latLng,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'storeType': storeType?.toMap(),
      'latLng': {"latitude": latLng.latitude, "longitude": latLng.longitude},
    };
  }

  factory Shop.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Shop(
      id: map['id'],
      name: map['name'],
      storeType: StoreType.fromMap(map['storeType']),
      latLng: LatLng(map['latLng']['latitude'], map['latLng']['longitude']),
    );
  }

  String toJson() => json.encode(toMap());

  factory Shop.fromJson(String source) => Shop.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Shop(id: $id, name: $name, storeType: $storeType, latLng: $latLng)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Shop &&
        o.id == id &&
        o.name == name &&
        o.storeType == storeType &&
        o.latLng == latLng;
  }

  @override
  int get hashCode {
    return id.hashCode ^ name.hashCode ^ storeType.hashCode ^ latLng.hashCode;
  }
}
