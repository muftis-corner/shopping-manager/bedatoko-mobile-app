import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:bedatoko/model/shop/shop.dart';

class Record {
  int id;
  List<int> productIds;
  Shop shop;
  DateTime createdDate;
  double price;
  Record({
    this.id,
    this.productIds,
    this.shop,
    this.createdDate,
    this.price,
  });

  Record copyWith({
    int id,
    List<int> productIds,
    Shop shop,
    DateTime createdDate,
    double price,
  }) {
    return Record(
      id: id ?? this.id,
      productIds: productIds ?? this.productIds,
      shop: shop ?? this.shop,
      createdDate: createdDate ?? this.createdDate,
      price: price ?? this.price,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'productIds': productIds,
      'shop': shop?.toMap(),
      'createdDate': createdDate?.millisecondsSinceEpoch,
      'price': price,
    };
  }

  factory Record.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Record(
      id: map['id'],
      productIds: List<int>.from(map['productIds']),
      shop: Shop.fromMap(map['shop']),
      createdDate: DateTime.fromMillisecondsSinceEpoch(map['createdDate']),
      price: map['price'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Record.fromJson(String source) => Record.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Record(id: $id, productIds: $productIds, shop: $shop, createdDate: $createdDate, price: $price)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Record &&
        o.id == id &&
        listEquals(o.productIds, productIds) &&
        o.shop == shop &&
        o.createdDate == createdDate &&
        o.price == price;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        productIds.hashCode ^
        shop.hashCode ^
        createdDate.hashCode ^
        price.hashCode;
  }
}
