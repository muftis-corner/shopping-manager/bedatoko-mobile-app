import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:bedatoko/constant/theme/theme.dart';

class RoundedTextField extends StatelessWidget {
  final TextEditingController controller;
  final String placeholder;
  final bool password;
  final FocusNode focus;
  final VoidCallback onEditingComplete;
  final TextInputAction textInputAction;
  final ValueChanged onChanged;
  final Widget icon;
  const RoundedTextField({
    Key key,
    this.controller,
    this.placeholder,
    this.password = false,
    this.focus,
    this.onEditingComplete,
    this.icon,
    this.onChanged,
    this.textInputAction,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoTextField(
        controller: this.controller,
        focusNode: this.focus,
        onChanged: this.onChanged,
        onEditingComplete: this.onEditingComplete,
        obscureText: this.password,
        style: ThemeText.text.copyWith(color: themeColor.text.withOpacity(0.6)),
        decoration: BoxDecoration(
          color: themeColor.backgroundAccent,
          borderRadius: BorderRadius.circular(16),
          border: Border.all(color: themeColor.secondary),
        ),
        prefix: Padding(
            padding: const EdgeInsets.only(left: 8.0), child: this.icon),
        placeholder: this.placeholder);
  }
}
