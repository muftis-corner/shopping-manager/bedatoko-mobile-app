import 'package:bedatoko/constant/theme/theme.dart';
import 'package:flutter/material.dart';

class ListHorizontalProduct extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 130,
      child: Card(
        elevation: 2,
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Column(
            children: [
              Expanded(
                flex: 2,
                child: Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(
                            'https://picsum.photos/id/239/200/300'),
                      ),
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(4)),
                ),
              ),
              Expanded(
                flex: 2,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Rinso Cair Lavender 1L',
                      style: ThemeText.titleS.copyWith(
                          fontSize: 12,
                          color: themeColor.text,
                          fontWeight: FontWeight.w700),
                    ),
                    Row(
                      children: [
                        Text(
                          'Rp 19.000',
                          style: ThemeText.titleS.copyWith(
                              color: themeColor.secondary,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.location_on_outlined,
                          color: themeColor.text,
                          size: 14,
                        ),
                        Text(
                          'Lotte Grosir Pal...',
                          style: ThemeText.titleS.copyWith(
                              fontSize: 12,
                              color: themeColor.text,
                              fontWeight: FontWeight.w600),
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
