import 'package:bedatoko/constant/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ThemedWidget {
  static AppBar appBar({String title, List<Widget> actions}) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    return AppBar(
      brightness: Brightness.light,
      elevation: 0.5,
      backgroundColor: themeColor.background,
      title: Text(
        '$title',
        style: ThemeText.titleM,
      ),
      actions: actions,
    );
  }
}
