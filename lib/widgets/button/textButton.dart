import 'package:flutter/material.dart';

import 'package:bedatoko/constant/theme/radius.dart';
import 'package:bedatoko/constant/theme/theme.dart';

class KTextButton extends StatelessWidget {
  final String text;
  final IconData icon;
  final Function onPressed;
  final Color color;
  KTextButton({
    Key key,
    @required this.text,
    @required this.onPressed,
    this.icon,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      child: Row(
        children: [
          if (this.icon != null) ...[
            Icon(
              this.icon,
              size: 18,
              color: Colors.white,
            ),
            SizedBox(
              width: 8,
            ),
          ],
          Text(
            '${this.text}',
            style: ThemeText.theme.button,
          ),
        ],
      ),
      color: this.color ?? themeColor.secondary,
      onPressed: onPressed,
      shape: RoundedRectangleBorder(borderRadius: ThemeRadius.all20),
    );
  }
}

class KButton extends StatelessWidget {
  final String text;
  final Widget icon;
  final Function onPressed;
  final Color color;
  final Color textColor;
  final Color borderColor;
  final double borderWidth;
  KButton({
    Key key,
    @required this.text,
    this.icon,
    @required this.onPressed,
    this.color,
    this.borderColor,
    this.borderWidth,
    this.textColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (this.icon != null) ...[
            this.icon,
            SizedBox(
              width: 8,
            ),
          ],
          Text(
            '${this.text}',
            style: ThemeText.theme.button
                .copyWith(color: this.textColor ?? themeColor.text),
          ),
        ],
      ),
      color: this.color ?? themeColor.secondary,
      onPressed: onPressed,
      shape: RoundedRectangleBorder(
        borderRadius: ThemeRadius.all20,
        side: BorderSide(
          color: this.borderColor ?? themeColor.primary,
          width: this.borderWidth ?? 0.0,
        ),
      ),
    );
  }
}
