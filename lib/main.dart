import 'package:camera/camera.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'constant/config.dart';
import 'constant/routes.dart';
import 'constant/theme/ThemeColor.dart';
import 'constant/theme/theme.dart';
import 'database/src/dataStore.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  Config.cameras = await availableCameras();
  DataStore.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        textTheme: ThemeText.theme,
        textSelectionColor: themeColor.text,
        primarySwatch: Colors.deepPurple,
        primaryColor: themeColor.primary,
        accentColor: themeColor.secondary,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      routes: Routes.routes(context),
      initialRoute: Routes.root,
    );
  }
}
