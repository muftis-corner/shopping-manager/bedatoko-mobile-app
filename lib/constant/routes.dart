import 'package:bedatoko/module/auth/splash/splash.dart';
import 'package:bedatoko/module/auth/welcome/welcome.dart';
import 'package:bedatoko/module/dashboard/main.dart';
import 'package:flutter/material.dart';

class Routes {
  static const String root = '/';
  static const String welcome = '/welcome';
  static const String dashboard = '/dashboard';

  static Map<String, WidgetBuilder> routes(BuildContext context) => {
        Routes.root: (context) => SplashScreen(),
        Routes.dashboard: (context) => Dashboard(),
        Routes.welcome: (context) => WelcomePage(),
      };
}
