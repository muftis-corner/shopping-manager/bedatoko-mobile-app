import 'package:bedatoko/constant/theme/theme.dart';
import 'package:flutter/material.dart';

class ThemeText {
  static TextTheme theme = TextTheme(
    bodyText1: ThemeText.text.copyWith(fontWeight: FontWeight.w400),
    bodyText2: ThemeText.text.copyWith(fontWeight: FontWeight.w500),
    headline1: ThemeText.titleM.copyWith(
        fontSize: 112, fontWeight: FontWeight.w100, color: themeColor.text),
    headline2: ThemeText.titleM.copyWith(
        fontSize: 56, fontWeight: FontWeight.w400, color: themeColor.text),
    headline3: ThemeText.titleM.copyWith(
        fontSize: 45, fontWeight: FontWeight.w400, color: themeColor.text),
    headline4: ThemeText.titleM.copyWith(
        fontSize: 34, fontWeight: FontWeight.w400, color: themeColor.text),
    headline5:
        ThemeText.titleM.copyWith(fontSize: 24, fontWeight: FontWeight.w500),
    headline6: ThemeText.titleM.copyWith(
        fontSize: 20, fontWeight: FontWeight.w400, color: themeColor.text),
    subtitle1:
        ThemeText.text.copyWith(fontSize: 16, fontWeight: FontWeight.w500),
    subtitle2:
        ThemeText.text.copyWith(fontSize: 14, fontWeight: FontWeight.w500),
    button:
        ThemeText.buttonS.copyWith(fontSize: 14, fontWeight: FontWeight.w500),
    caption: ThemeText.hint.copyWith(fontSize: 12, fontWeight: FontWeight.w500),
    overline:
        ThemeText.titleL.copyWith(fontSize: 10, fontWeight: FontWeight.w400),
  );

  static TextStyle titleS = TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.bold,
      color: themeColor.title,
      fontFamily: "Nunito");
  static TextStyle titleM = TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.bold,
      color: themeColor.title,
      fontFamily: "Nunito");
  static TextStyle titleL = TextStyle(
      fontSize: 24,
      fontWeight: FontWeight.bold,
      color: themeColor.title,
      fontFamily: "Nunito");
  static TextStyle buttonS = TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.bold,
      color: themeColor.backgroundAccent,
      fontFamily: "Nunito");
  static TextStyle buttonM = TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.bold,
      color: themeColor.backgroundAccent,
      fontFamily: "Nunito");
  static TextStyle buttonL = TextStyle(
      fontSize: 22,
      fontWeight: FontWeight.bold,
      color: themeColor.backgroundAccent,
      fontFamily: "Nunito");

  static TextStyle form = TextStyle(
    fontSize: 16,
    fontFamily: "Nunito",
  );

  static TextStyle hint =
      TextStyle(fontSize: 12, fontFamily: "Nunito", color: themeColor.hint);

  static TextStyle text =
      TextStyle(fontSize: 14, fontFamily: "Nunito", color: themeColor.text);
}
