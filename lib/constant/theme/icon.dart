import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ThemeIcon {
  static const IconData home = FontAwesomeIcons.home;
  static const IconData search = FontAwesomeIcons.search;
  static const IconData cart = FontAwesomeIcons.shoppingCart;
  static const IconData store = FontAwesomeIcons.store;
  static const IconData me = FontAwesomeIcons.userAlt;
  static const IconData filter = FontAwesomeIcons.filter;
}

class ImageIcons {
  static const ImageIcon google = ImageIcon('assets/icon/google.png');
  static const ImageIcon lock = ImageIcon('assets/icon/Lock.png');
  static const ImageIcon user = ImageIcon('assets/icon/Profile.png');
  static const ImageIcon message = ImageIcon('assets/icon/Message.png');
}

class ImageIcon extends StatelessWidget {
  final String asset;
  final double size;

  const ImageIcon(this.asset, {this.size = 18});

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      this.asset,
      scale: 1.0,
      height: this.size,
      width: this.size,
    );
  }
}
