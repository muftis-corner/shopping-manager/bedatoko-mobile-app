import 'package:flutter/rendering.dart';

class ThemeRadius {
  static const Radius rad36 = const Radius.circular(36);
  static const Radius rad32 = const Radius.circular(32);
  static const Radius rad20 = const Radius.circular(20);
  static const Radius rad16 = const Radius.circular(16);
  static const Radius rad12 = const Radius.circular(12);
  static const Radius rad8 = Radius.circular(8);

  static const BorderRadius borderLeft32 =
      BorderRadius.only(topLeft: rad36, bottomLeft: rad36);

  static const BorderRadius borderRight12 =
      BorderRadius.only(topRight: rad12, bottomRight: rad12);

  static const BorderRadius borderRight20 =
      BorderRadius.only(topRight: rad20, bottomRight: rad20);

  static const BorderRadius borderTopRight32 =
      BorderRadius.only(topRight: rad32);

  static const BorderRadius borderTopRight20 =
      BorderRadius.only(topRight: rad20);

  static const BorderRadius searchBar =
      BorderRadius.only(topRight: rad20, bottomRight: rad20);

  static const BorderRadius all12 = BorderRadius.all(rad12);
  static const BorderRadius all20 = BorderRadius.all(rad20);
  static const BorderRadius all32 = BorderRadius.all(rad32);
}
