import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';

ThemeColor themeColor = ThemeColor.theme['light'];

class ThemeColor {
  final Color primary;
  final Color secondary;
  final Color primaryAccent;
  final Color title;
  final Color hint;
  final Color text;
  final Color background;
  final Color backgroundAccent;
  ThemeColor({
    this.primary,
    this.secondary,
    this.primaryAccent,
    this.title,
    this.hint,
    this.text,
    this.background,
    this.backgroundAccent,
  });

  static Map<String, ThemeColor> theme = {
    "light": ThemeColor(
        primary: Color(0xFF2A2D45),
        primaryAccent: Color(0xFF4D5373),
        secondary: Color(0xFFE66535),
        title: Color(0xFFFFFFFF),
        hint: Color(0xFFC4C4C4),
        text: Color(0xFF222437),
        backgroundAccent: Color(0xFFFFFFFF),
        background: Color(0xFFEBF0F3))
  };

  ThemeColor copyWith({
    Color primary,
    Color secondary,
    Color primaryAccent,
    Color title,
    Color hint,
    Color text,
    Color background,
    Color backgroundAccent,
  }) {
    return ThemeColor(
      primary: primary ?? this.primary,
      secondary: secondary ?? this.secondary,
      primaryAccent: primaryAccent ?? this.primaryAccent,
      title: title ?? this.title,
      hint: hint ?? this.hint,
      text: text ?? this.text,
      background: background ?? this.background,
      backgroundAccent: backgroundAccent ?? this.backgroundAccent,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'primary': primary?.value,
      'secondary': secondary?.value,
      'primaryAccent': primaryAccent?.value,
      'title': title?.value,
      'hint': hint?.value,
      'text': text?.value,
      'background': background?.value,
      'backgroundAccent': backgroundAccent?.value,
    };
  }

  factory ThemeColor.fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return ThemeColor(
      primary: Color(map['primary']),
      secondary: Color(map['secondary']),
      primaryAccent: Color(map['primaryAccent']),
      title: Color(map['title']),
      hint: Color(map['hint']),
      text: Color(map['text']),
      background: Color(map['background']),
      backgroundAccent: Color(map['backgroundAccent']),
    );
  }

  String toJson() => json.encode(toMap());

  factory ThemeColor.fromJson(String source) =>
      ThemeColor.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ThemeColor(primary: $primary, secondary: $secondary, primaryAccent: $primaryAccent, title: $title, hint: $hint, text: $text, background: $background, backgroundAccent: $backgroundAccent)';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is ThemeColor &&
        o.primary == primary &&
        o.secondary == secondary &&
        o.primaryAccent == primaryAccent &&
        o.title == title &&
        o.hint == hint &&
        o.text == text &&
        o.background == background &&
        o.backgroundAccent == backgroundAccent;
  }

  @override
  int get hashCode {
    return primary.hashCode ^
        secondary.hashCode ^
        primaryAccent.hashCode ^
        title.hashCode ^
        hint.hashCode ^
        text.hashCode ^
        background.hashCode ^
        backgroundAccent.hashCode;
  }
}
