import 'package:bedatoko/constant/theme/theme.dart';
import 'package:flutter/material.dart';

class ThemeGradient {
  static Gradient background = LinearGradient(
    colors: [
      themeColor.primary,
      themeColor.primary,
      themeColor.primaryAccent,
    ],
    // stops: [0.0, 0.9],
    begin: FractionalOffset.topCenter,
    end: FractionalOffset.bottomCenter,
  );
}
