import 'package:bedatoko/constant/theme/theme.dart';
import 'package:bedatoko/module/dashboard/model/menuItem.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

List<List> _navbarMenuRaw = [
  ['Home', FontAwesomeIcons.home],
  ['Store', FontAwesomeIcons.store],
  ['', null],
  ['My List', FontAwesomeIcons.shoppingBasket],
  ['Me', FontAwesomeIcons.user],
];

List<MenuItem> dashboardNavbarMenuItem = _navbarMenuRaw
    .map((instance) => MenuItem(
          title: instance[0],
          icons: instance[1],
        ))
    .toList();
