import 'package:camera/camera.dart';

class Config {
  static const API_URL = 'http://147.139.184.228:9354';
  static const API_TIMEOUT = 3;
  static List<CameraDescription> cameras;
}

class ApiName {
  static const category = '/category';
  static const brand = '/brand';
  static const currency = '/currency';
  static const item = '/item';
  static const net = '/net';
  static const product = '/product';
  static const record = '/record';
  static const shop = '/shop';
}
