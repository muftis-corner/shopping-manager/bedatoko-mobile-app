import 'package:bedatoko/constant/config.dart';
import 'package:bedatoko/constant/response/response.dart';
import 'package:bedatoko/data/session.dart';
import 'package:dio/dio.dart';

class API {
  static Dio _dio = Dio();
  static const URL = Config.API_URL;

  static Options _options = Options(headers: {
    // "Content-Type": "application/x-www-form-urlencoded",
    "Authorization": "bearer ${Session.token}"
  });

  static Future<Response> get(String url, {Map<String, dynamic> params}) async {
    try {
      final Response response = await _dio
          .get(
        url,
        queryParameters: params,
        options: _options,
        onReceiveProgress: (count, total) => print('$count, $total,'),
      )
          .timeout(Duration(seconds: Config.API_TIMEOUT), onTimeout: () {
        throw Exception('Request Timed out');
      });
      return response;
    } catch (e) {
      return Response(data: ResponseMessage.error(e));
    }
  }

  static Future<Response> login(String url, Map<String, dynamic> data) async {
    try {
      final Response response = await _dio
          .post(
        url,
        data: data,
      )
          .timeout(Duration(seconds: Config.API_TIMEOUT), onTimeout: () {
        throw Exception('Request Timed out');
      });
      return response;
    } catch (e) {
      return Response(data: ResponseMessage.error(e));
    }
  }

  static Future<Response> post(String url, Map<String, dynamic> data) async {
    try {
      final Response response = await _dio
          .post(
        url,
        data: data,
        options: _options,
      )
          .timeout(Duration(seconds: Config.API_TIMEOUT), onTimeout: () {
        throw Exception('Request Timed out');
      });
      return response;
    } catch (e) {
      return Response(data: ResponseMessage.error(e));
    }
  }

  static Future<Response> postFormData(String url, FormData data) async {
    try {
      final Response response = await _dio
          .post(
        url,
        data: data,
        options: _options,
      )
          .timeout(Duration(seconds: Config.API_TIMEOUT), onTimeout: () {
        throw Exception('Request Timed out');
      });
      return response;
    } catch (e) {
      return Response(data: ResponseMessage.error(e));
    }
  }
}
